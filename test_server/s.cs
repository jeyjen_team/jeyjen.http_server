﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace test_server
{
    class s
    {
        public void start()
        {
            // URI prefixes are required,
            // for example "http://contoso.com:8080/index/".

            
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:10002/");
            listener.Start();
            // Specify Negotiate as the authentication scheme.
            listener.AuthenticationSchemes = AuthenticationSchemes.Negotiate;
            // If NTLM is used, we will allow multiple requests on the same
            // connection to use the authentication information of first request.
            // This improves performance but does reduce the security of your 
            // application. 
            listener.UnsafeConnectionNtlmAuthentication = true;
            // This listener does not want to receive exceptions 
            // that occur when sending the response to the client.
            listener.IgnoreWriteExceptions = true;

            while (true)
            {
                var ctx = listener.GetContext();
                var data = Encoding.UTF8.GetBytes("werdas");
                ctx.Response.OutputStream.Write(data, 0, data.Length);
                
            }
            Console.WriteLine("Listening...");

            listener.Close();
        }

    }
}
