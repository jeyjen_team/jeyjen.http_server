﻿using jeyjen.net.server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace test_server
{
    public class test_server : web_server
    {
        public test_server(int port, X509Certificate2 certificate = null) : base(port, certificate)
        { }
        protected override void on_http_request(context context)
        {
            context.responce.status = status.OK;
            context.responce.send("hello world");
        }

        protected override void on_websocket_connect(websocket socket)
        {
            base.on_websocket_connect(socket);
            //socket.send(":: okey no!");
        }

        protected override void on_websocket_disconnect(websocket socket, string reason)
        {
            base.on_websocket_disconnect(socket, reason);
        }

        protected override void on_websocket_pong(websocket socket, string message)
        {
            base.on_websocket_pong(socket, message);
            socket.send(ws_opcode.pong, Encoding.UTF8.GetBytes(""));
        }

        protected override void on_data(websocket socket, byte[] data, bool last_frame)
        {
            base.on_data(socket, data, last_frame);
        }

        protected override void on_message(websocket socket, string message, bool last_frame)
        {
            //socket.send(message + ":: okey no!");
        }
    }
}
