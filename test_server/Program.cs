﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_server
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Chilkat.Ntlm ntlmClient = new Chilkat.Ntlm();

            bool success = ntlmClient.UnlockComponent("Anything for 30-day trial");
            if (success != true)
            {
                Console.WriteLine(ntlmClient.LastErrorText);
                return;
            }

            //  UnlockComponent only needs to be called once on the 1st object instance.

            Chilkat.Ntlm ntlmServer = new Chilkat.Ntlm();

            //  The NTLM protocol begins by the client sending the server
            //  a Type1 message.
            string type1Msg;
            ntlmClient.Workstation = "MyWorkstation";
            type1Msg = ntlmClient.GenType1();

            Console.WriteLine("Type1 message from client to server:");
            Console.WriteLine(type1Msg);

            //  If the server wishes to examine the information embedded within the
            //  Type1 message, it may call ParseType1.
            //  This step is not necessary, it is only for informational purposes..
            string type1Info = ntlmServer.ParseType1(type1Msg);

            Console.WriteLine("---");
            Console.WriteLine(type1Info);

            //  The server now generates a Type2 message to be sent to the client.
            //  The Type2 message requires a TargetName.  A TargetName is
            //  the authentication realm in which the authenticating account
            //  has membership (a domain name for domain accounts, or server name
            //  for local machine accounts).
            ntlmServer.TargetName = "myAuthRealm";

            string type2Msg = ntlmServer.GenType2(type1Msg);
            if (ntlmServer.LastMethodSuccess != true)
            {
                Console.WriteLine(ntlmServer.LastErrorText);
                return;
            }

            Console.WriteLine("Type2 message from server to client:");
            Console.WriteLine(type2Msg);

            //  The client may examine the information embedded in the Type2 message
            //  by calling ParseType2, which returns XML.  This is only for informational purposes
            //  and is not required.
            string type2Info = ntlmClient.ParseType2(type2Msg);

            Console.WriteLine("---");
            Console.WriteLine(type2Info);

            //  The client will now generate the final Type3 message to be sent to the server.
            //  This requires the Username and Password:
            ntlmClient.UserName = "test123";
            ntlmClient.Password = "myPassword";

            string type3Msg;
            type3Msg = ntlmClient.GenType3(type2Msg);
            if (ntlmClient.LastMethodSuccess != true)
            {
                Console.WriteLine(ntlmClient.LastErrorText);
                return;
            }

            Console.WriteLine("Type3 message from client to server:");
            Console.WriteLine(type3Msg);

            //  The server may verify the response by first "loading" the Type3 message.
            //  This sets the various properties such as Username, Domain, Workstation,
            //  and ClientChallenge to the values embedded within theType3 message.
            //  The server may then use the Username to lookup the password.
            //  Looking up the password is dependent on your infrastructure.  Perhaps your
            //  usernames/passwords are stored in a secure database.  If that's the case, you would
            //  write code to issue a query to get the password string for the given username.
            //  Once the password is obtained, set the Password property and then
            //  generate the Type3 response again.  If the server's Type3 response matches
            //  the client's Type3 response, then the client's password is correct.

            success = ntlmServer.LoadType3(type3Msg);
            if (success != true)
            {
                Console.WriteLine(ntlmServer.LastErrorText);
                return;
            }

            //  The Username property now contains the username that was embedded within
            //  the Type3 message.  It can be used to lookup the password.
            string clientUsername = ntlmServer.UserName;

            //  For this example, we'll simply set the password to a literal string:
            ntlmServer.Password = "myPassword";

            //  The server may generate the Type3 message again, using the client's correct
            //  password:
            string expectedType3Msg = ntlmServer.GenType3(type2Msg);

            Console.WriteLine("Expected Type3 Message:");
            Console.WriteLine(expectedType3Msg);
            */

            /*
            var _server = new test_server(10002);
            _server.start();
            Console.WriteLine("port 10002");
            Console.ReadLine();
            */

            //var dsa = new s();
            //dsa.start();

            var tb1 = Convert.FromBase64String("TlRMTVNTUAABAAAAl4II4gAAAAAAAAAAAAAAAAAAAAAGAbEdAAAADw==");
            var tb2 = Convert.FromBase64String("TlRMTVNTUAACAAAADgAOADgAAAAVgoni8YSR0XrPO8kAAAAAAAAAAJYAlgBGAAAABgGxHQAAAA9PAFAARQBOAC4AUgBVAAIADgBPAFAARQBOAC4AUgBVAAEAHABDAFQATwAtAE4ATwBWAEkASwBPAFYALQBFAE8ABAAOAG8AcABlAG4ALgByAHUAAwAsAGMAdABvAC0AbgBvAHYAaQBrAG8AdgAtAGUAbwAuAG8AcABlAG4ALgByAHUABQAOAG8AcABlAG4ALgByAHUABwAIANcHFykVidIBAAAAAA==");
            var tb3 = Convert.FromBase64String("TlRMTVNTUAADAAAAGAAYAIgAAAA6AToBoAAAAAAAAABYAAAAFAAUAFgAAAAcABwAbAAAABAAEADaAQAAFYKI4gYBsR0AAAAPFZ1xQcGzqBVLQLLcHpuBtW4AbwB2AGkAawBvAHYAXwBlAG8AQwBUAE8ALQBOAE8AVgBJAEsATwBWAC0ARQBPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIaSh623BW9sI4qnw02ba1cBAQAAAAAAANcHFykVidIBm6HZ/z1319IAAAAAAgAOAE8AUABFAE4ALgBSAFUAAQAcAEMAVABPAC0ATgBPAFYASQBLAE8AVgAtAEUATwAEAA4AbwBwAGUAbgAuAHIAdQADACwAYwB0AG8ALQBuAG8AdgBpAGsAbwB2AC0AZQBvAC4AbwBwAGUAbgAuAHIAdQAFAA4AbwBwAGUAbgAuAHIAdQAHAAgA1wcXKRWJ0gEGAAQAAgAAAAgAMAAwAAAAAAAAAAAAAAAAIAAAb0uxXlsEnlqjCJLVL2IsQsgKOsY4FJ/JZGJMn2dC6HcKABAAAAAAAAAAAAAAAAAAAAAAAAkAHABIAFQAVABQAC8AbABvAGMAYQBsAGgAbwBzAHQAAAAAAAAAAAAAAAAAtWsrCr5OkFxLas+YIDIlOA==");

            var s1 = Encoding.UTF8.GetString(tb1);
            var s2 = Encoding.UTF8.GetString(tb2);
            var s3 = Encoding.UTF8.GetString(tb3);

        }
    }
}
