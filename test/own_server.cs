﻿using jeyjen.net.server;
using System.Text;
using System.Security.Cryptography.X509Certificates;

namespace test
{
    public class own_server : web_server
    {
        public own_server(int port, X509Certificate2 certificate = null) : base(port, certificate)
        {}
        protected override void on_http_request(context context)
        {
            context.responce.status = status.OK;
            context.responce.send("hello world");
        }

        protected override void on_websocket_connect(websocket socket)
        {
            base.on_websocket_connect(socket);
        }

        protected override void on_websocket_disconnect(websocket socket, string reason)
        {
            base.on_websocket_disconnect(socket, reason);
        }

        protected override void on_websocket_pong(websocket socket, string message)
        {
            base.on_websocket_pong(socket, message);
        }

        protected override void on_data(websocket socket, byte[] data, bool last_frame)
        {
            base.on_data(socket, data, last_frame);
        }

        protected override void on_message(websocket socket, string message, bool last_frame)
        {
            socket.send(message + ":: okey no!");
        }
    }
}
