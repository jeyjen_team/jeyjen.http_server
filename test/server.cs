﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using WebSocket4Net;
using SuperSocket.ClientEngine;
using jeyjen.extension;

namespace test
{
    [TestClass]
    public class server
    {
        private own_server _server;
        private WebSocket _websocket;
        private int _port = 11076;

        [TestInitialize]
        public void init()
        {
            _server = new own_server(_port);
            _server.start();
            Thread.Sleep(3000);
        }

        [TestCleanup]
        public void clean()
        {
            _server.stop();
        }

        [TestMethod]
        public void server_creation()
        {
            //var cert = new X509Certificate2("D:\\prjs\\_cert\\server_cert.pfx", "Test123");
            _websocket = new WebSocket("ws://localhost:{0}/".format(_port));
            _websocket.Opened += new EventHandler(websocket_Opened);
            _websocket.Error += new EventHandler<ErrorEventArgs>(websocket_Error);
            _websocket.Closed += new EventHandler(websocket_Closed);
            _websocket.MessageReceived += Websocket_MessageReceived;
            _websocket.Open();
            while(true)
            {
                Thread.Sleep(1000);
            }
        }

        private void Websocket_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
           
        }

        private void websocket_Closed(object sender, EventArgs e)
        {
            
        }

        private void websocket_Error(object sender, ErrorEventArgs e)
        {
           
        }

        private void websocket_Opened(object sender, EventArgs e)
        {
            _websocket.Send("Hello World!");
        }

    }
}
