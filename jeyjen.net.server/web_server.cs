﻿using jeyjen.extension;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Web;

namespace jeyjen.net.server
{
    internal enum block
    {
        method,
        url,
        url_key,
        url_value,
        version,
        header_key,
        header_value,
        body,
        end
    };

    public abstract class web_server
    {
        TcpListener _listener;
        X509Certificate2 _certificate;
        LinkedList<connection> _http_clients;
        LinkedList<websocket> _ws_clients;
        private bool _is_alive;
        private ws_opcode _multi_frame_opcode;

        public web_server(int port, X509Certificate2 certificate = null)
        {
            _listener = new TcpListener(IPAddress.Any, port);
            _certificate = certificate;
            _is_alive = false;
            _http_clients = new LinkedList<connection>();
            _ws_clients = new LinkedList<websocket>();
        }

        public void start()
        {
            _is_alive = true;
            var accept_thread = new Thread(() => 
            {
                _listener.Start();
                
                while (true)
                {
                    try
                    {
                        var client = _listener.AcceptTcpClient();
                        lock (_http_clients)
                        {
                            _http_clients.AddLast(new connection(client));
                        }
                    }
                    catch (IOException e)
                    {
                        // Тогда корректное завершение работы
                    }
                    catch (Exception e)
                    {
                    }
                }
                
            });
            accept_thread.IsBackground = true;

            var http_thread = new Thread(()=> 
            {
                var buffer = new byte[1024 * 10];
                var method = new StringBuilder();
                var url = new StringBuilder();
                var url_param = new Dictionary<string, string>();
                var header = new Dictionary<string, string>();
                var version = new StringBuilder();
                int body_size = 0;
                //List<byte[]> body = new List<byte[]>();
                byte[] body = null;

                var state = block.method;
                int read = 0;
                int ndx;
                StringBuilder key = null;
                StringBuilder value = null;
                char ch;
                //int n_to_read = buffer.Length;

                connection con = null;
                Stream s = null;
                LinkedListNode<connection> el = null;
                LinkedListNode<connection> tmp = null;
                var read_available = new Queue<connection>();
                while (_is_alive)
                {
                    try
                    {
                        el = _http_clients.First;
                        // цикл проверки подключений
                        while (el != null)
                        {
                            con = el.Value;
                            if (con.is_alive)
                            {
                                if (con.client.Available > 0)
                                {
                                    read_available.Enqueue(con);
                                }
                                el = el.Next;
                            }
                            else
                            {
                                tmp = el;
                                el = el.Next;
                                _http_clients.Remove(tmp);
                            }
                        }

                        if (read_available.Count > 0)
                        {
                            while (read_available.Count > 0)
                            {
                                try
                                {
                                    con = read_available.Dequeue();
                                    s = con.stream;
                                    while (con.client.Available > 0)
                                    {
                                        try
                                        {
                                            read = s.Read(buffer, 0, con.client.Available);
                                            if (read == 0)
                                            {
                                                continue;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            continue;
                                        }
                                        ndx = 0;
                                        #region parse
                                        while (ndx < read)
                                        {
                                            ch = (char)buffer[ndx];
                                            switch (state)
                                            {
                                                #region method
                                                case block.method:
                                                    {
                                                        if (ch != ' ')
                                                        {
                                                            method.Append(ch);
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            state = block.url;
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region url
                                                case block.url:
                                                    {
                                                        if (ch == '?')
                                                        {
                                                            key = new StringBuilder();
                                                            state = block.url_key;
                                                            ndx++;
                                                        }
                                                        else if (ch != ' ')
                                                        {
                                                            url.Append(ch);
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            state = block.version;
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region url_key
                                                case block.url_key:
                                                    {
                                                        if (ch == '=')
                                                        {
                                                            value = new StringBuilder();
                                                            state = block.url_value;
                                                            ndx++;
                                                        }
                                                        else if (ch == ' ')
                                                        {
                                                            state = block.version;
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            key.Append(ch);
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region url_value
                                                case block.url_value:
                                                    {
                                                        if (ch == '&')
                                                        {
                                                            var k = HttpUtility.UrlDecode(key.ToString());
                                                            if (url_param.ContainsKey(k))
                                                            {
                                                                url_param[k] = url_param[k] + "," + HttpUtility.UrlDecode(value.ToString());
                                                            }
                                                            else
                                                            {
                                                                url_param.Add(k, HttpUtility.UrlDecode(value.ToString()));
                                                            }
                                                            key = new StringBuilder();
                                                            state = block.url_key;
                                                            ndx++;
                                                        }
                                                        else if (ch == ' ')
                                                        {
                                                            ndx++;
                                                            var k = HttpUtility.UrlDecode(key.ToString());
                                                            if (url_param.ContainsKey(k))
                                                            {
                                                                url_param[k] = url_param[k] + "," + HttpUtility.UrlDecode(value.ToString());
                                                            }
                                                            else
                                                            {
                                                                url_param.Add(k, HttpUtility.UrlDecode(value.ToString()));
                                                            }
                                                            state = block.version;
                                                        }
                                                        else
                                                        {
                                                            value.Append(ch);
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region version
                                                case block.version:
                                                    {
                                                        if (ch == '\r')
                                                        {
                                                            ndx++;
                                                        }
                                                        else if (ch != '\n')
                                                        {
                                                            version.Append(ch);
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            key = new StringBuilder();
                                                            state = block.header_key;
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region header_key
                                                case block.header_key:
                                                    {
                                                        if (ch == '\r')
                                                        {
                                                            ndx++;
                                                        }
                                                        else if (ch == '\n')
                                                        {
                                                            if (header.ContainsKey("content-length"))
                                                            {
                                                                body_size = Convert.ToInt32(header["content-length"]);
                                                                state = block.body;
                                                                ndx++;
                                                            }
                                                            else
                                                            {
                                                                state = block.end;
                                                            }
                                                        }
                                                        else if (ch == ':')
                                                        {
                                                            ndx++;
                                                        }
                                                        else if (ch != ' ')
                                                        {
                                                            key.Append(ch);
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            ndx++;
                                                            value = new StringBuilder();
                                                            state = block.header_value;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region header_value
                                                case block.header_value:
                                                    {
                                                        if (ch == '\r')
                                                        {
                                                            ndx++;
                                                        }
                                                        else if (ch != '\n')
                                                        {
                                                            value.Append(ch);
                                                            ndx++;
                                                        }
                                                        else
                                                        {
                                                            header.Add(key.ToString().ToLower(), value.ToString());
                                                            key = new StringBuilder();
                                                            state = block.header_key;
                                                            ndx++;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                #region body
                                                case block.body:
                                                    {
                                                        body = new byte[body_size];
                                                        Array.Copy(buffer, ndx, body, 0, body.Length);
                                                        state = block.end;
                                                        /*
                                                        byte[] pack = null;
                                                        if (n_to_read <= read - ndx)
                                                        {
                                                            pack = new byte[n_to_read];
                                                            n_to_read = buffer.Length;
                                                            
                                                        }
                                                        else
                                                        {
                                                            n_to_read = n_to_read - (read - ndx);
                                                            pack = new byte[read - ndx];
                                                        }
                                                        Array.Copy(buffer, ndx, pack, 0, pack.Length);
                                                        body.Add(pack);
                                                        
                                                        */
                                                    }
                                                    break;
                                                #endregion
                                                #region end
                                                case block.end:
                                                    {
                                                        //var b = new byte[body_size];
                                                        //int m_idx = 0;
                                                        //foreach (var p in body)
                                                        //{
                                                        //    foreach (var e in p)
                                                        //    {
                                                        //        b[m_idx++] = e;
                                                        //    }
                                                        //}
                                                        ndx = read;
                                                        var request = new request(method.ToString(), url.ToString(), version.ToString(), url_param, header, body);
                                                        var responce = new responce(con, version.ToString());
                                                        var context = new context(con, request, responce);
                                                        string sss = "";
                                                        if (request.headers.ContainsKey("sec-websocket-key"))
                                                        {
                                                            var ws = new websocket(con, request.url, request.host, request.origin, url_param, header);
                                                            lock (_http_clients)
                                                            {
                                                                _http_clients.Remove(con);
                                                            }
                                                            lock (_ws_clients)
                                                            {
                                                                _ws_clients.AddLast(ws);
                                                            }
                                                            var k = request.headers["sec-websocket-key"];
                                                            responce.status = status.Switching_Protocols;
                                                            responce["Connection"] = "Upgrade";
                                                            responce["Upgrade"] = "websocket";
                                                            responce["Sec-WebSocket-Accept"] = compute_accept_string(k);
                                                            responce["Sec-WebSocket-Version"] = "13";
                                                            //responce["Sec-WebSocket-Extensions"] = "permessage-deflate";
                                                            responce.send();
                                                            on_websocket_connect(ws);

                                                            state = block.method;
                                                            method.Clear();
                                                            url.Clear();
                                                            version.Clear();
                                                            body_size = 0;
                                                            url_param.Clear();
                                                            header.Clear();
                                                            body = null;
                                                        }
                                                        else if (request.headers.ContainsKey("authorization"))
                                                        {
                                                            var auth = request.headers["authorization"];
                                                            if (auth.StartsWith("NTLM "))
                                                            {
                                                                
                                                                byte[] msg = Convert.FromBase64String(auth.Substring(5));

                                                                

                                                                

                                                                var mmmmm = Encoding.ASCII.GetString(msg);
                                                                int off = 0, length, offset;
                                                                if (msg[8] == 1)
                                                                {
                                                                    off = 18;
                                                                    byte z = 0;
                                                                    byte[] msg1 = new byte[]
                                                                    {
                                                                        (byte)'N', (byte)'T', (byte)'L', (byte)'M',
                                                                        (byte)'S',(byte)'S', (byte)'P',z,
                                                                        2, z, z, z,
                                                                        z, z, z, z,
                                                                        40, z, z, z,
                                                                        1, 130, z, z,
                                                                        z, 2, 2, 2,
                                                                        z, z, z, z,
                                                                        z, z, z, z,
                                                                        z, z, z, z
                                                                    };
                                                                    context.responce["WWW-Authenticate"] = "NTLM " + Convert.ToBase64String(msg1).Trim();
                                                                    context.responce.status = status.Unauthorized;
                                                                    context.responce.send();
                                                                  
                                                                }
                                                                else if (msg[8] == 3)
                                                                {
                                                                    off = 30;
                                                                    length = msg[off + 17] * 256 + msg[off + 16];
                                                                    offset = msg[off + 19] * 256 + msg[off + 8];
                                                                    sss = Encoding.UTF8.GetString(msg, offset, length);
                                                                  
                                                                }
                                                                else
                                                                {
                                                                    return;
                                                                }

                                                                length = msg[off + 1] * 256 + msg[off];
                                                                offset = msg[off + 3] * 256 + msg[off + 2];
                                                                sss = Encoding.UTF8.GetString(msg, offset, length);

                                                                length = msg[off + 9] * 256 + msg[off + 8];
                                                                offset = msg[off + 11] * 256 + msg[off + 10];

                                                                sss = Encoding.UTF8.GetString(msg, offset, length);
                                                                
                                                                
                                                            }
                                                        }
                                                        else
                                                        {
                                                            context.responce["WWW-Authenticate"] = "NTLM";
                                                            context.responce.status = status.Unauthorized;
                                                            context.responce.send();
                                                            //on_http_request(context);
                                                            state = block.method;
                                                            method.Clear();
                                                            url.Clear();
                                                            version.Clear();
                                                            body_size = 0;
                                                            url_param.Clear();
                                                            header.Clear();
                                                            body = null;
                                                        }
                                                    }
                                                    break;
                                                #endregion
                                                default: break;
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        else
                        {
                            Thread.Sleep(300);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                  
            });
            http_thread.IsBackground = true;

            var ws_thread = new Thread(()=> 
            {
                byte fin_bit_flag = 0x80;
                byte op_code_flag = 0x0F;
                byte mask_flag = 0x80;
                byte byte_1;
                byte byte_2;
                byte[] decoded_payload;
                connection con = null;
                Stream stream = null;
                websocket ws = null;
                LinkedListNode<websocket> el = null;
                LinkedListNode<websocket> tmp = null;
                var read_available = new Queue<websocket>();
                while (_is_alive)
                {
                    try
                    {
                        el = _ws_clients.First;
                        while (el != null)
                        {
                            con = el.Value.connection;

                            if (con.is_alive)
                            {
                                if (con.client.Available > 0)
                                {
                                    read_available.Enqueue(el.Value);
                                }
                                el = el.Next;
                            }
                            else
                            {
                                tmp = el;
                                el = el.Next;
                                _ws_clients.Remove(tmp);
                            }
                        }

                        if (read_available.Count > 0)
                        {
                            while (read_available.Count > 0)
                            {
                                try
                                {
                                    ws = read_available.Dequeue();
                                    con = ws.connection;
                                    while (con.client.Available > 0)
                                    {
                                        stream = con.stream;
                                        byte_1 = (byte)con.stream.ReadByte();

                                        bool is_fin_bit_set = (byte_1 & fin_bit_flag) == fin_bit_flag;
                                        ws_opcode op_code = (ws_opcode)(byte_1 & op_code_flag);

                                        byte_2 = (byte)stream.ReadByte();

                                        bool is_mask_bit_set = (byte_2 & mask_flag) == mask_flag;
                                        uint len = read_length(byte_2, stream);


                                        // use the masking key to decode the data if needed
                                        if (is_mask_bit_set)
                                        {
                                            const int mask_key_len = 4;
                                            byte[] mask_key = stream.read(mask_key_len);
                                            byte[] encoded_payload = stream.read((int)len);
                                            decoded_payload = new byte[len];

                                            // apply the mask key
                                            for (int i = 0; i < encoded_payload.Length; i++)
                                            {
                                                decoded_payload[i] = (byte)(encoded_payload[i] ^ mask_key[i % mask_key_len]);
                                            }
                                        }
                                        else
                                        {
                                            decoded_payload = stream.read((int)len);
                                        }
                                        if (op_code == ws_opcode.continuation_frame)
                                        {
                                            switch (_multi_frame_opcode)
                                            {
                                                case ws_opcode.text_frame:
                                                    {
                                                        on_message(ws, Encoding.UTF8.GetString(decoded_payload, 0, decoded_payload.Length), is_fin_bit_set);
                                                    }
                                                    break;
                                                case ws_opcode.binary_frame:
                                                    {
                                                        on_data(ws, decoded_payload, is_fin_bit_set);
                                                    }
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            switch (op_code)
                                            {
                                                case ws_opcode.connection_close:
                                                    {
                                                        on_websocket_disconnect(ws, Encoding.UTF8.GetString(decoded_payload));
                                                        ws.close();
                                                    }
                                                    break;
                                                case ws_opcode.ping:
                                                    {
                                                        ws.send(ws_opcode.pong, Encoding.UTF8.GetBytes("pong"));
                                                    }
                                                    break;
                                                case ws_opcode.pong:
                                                    {
                                                        on_websocket_pong(ws, Encoding.UTF8.GetString(decoded_payload));
                                                    }
                                                    break;
                                                case ws_opcode.text_frame:
                                                    {
                                                        if (!is_fin_bit_set)
                                                        {
                                                            _multi_frame_opcode = op_code;
                                                        }
                                                        on_message(ws, Encoding.UTF8.GetString(decoded_payload, 0, decoded_payload.Length), is_fin_bit_set);
                                                    }
                                                    break;
                                                case ws_opcode.binary_frame:
                                                    {
                                                        if (!is_fin_bit_set)
                                                        {
                                                            _multi_frame_opcode = op_code;
                                                        }
                                                        on_data(ws, decoded_payload, is_fin_bit_set);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                }
                            }
                        }
                        else
                        {
                            Thread.Sleep(300);
                        }                              
                    }
                    catch (Exception ex)
                    {

                    }
                }
                // при закрытие соединения удаленным сокетом удалить его из списка сокетов
            });
            ws_thread.IsBackground = true;

            accept_thread.Start();
            http_thread.Start();
            ws_thread.Start();
        }

        public void stop()
        {
            _is_alive = false;
            _listener.Stop(); // чтобы больше не принимал запросов
            foreach (var c in _http_clients)
            {
                try
                {
                    c.close();
                }
                catch (Exception e)
                {}
            }
            foreach (var c in _ws_clients)
            {
                try
                {
                    c.close();
                }
                catch
                {
                }
            }
        }

        private static uint read_length(byte byte_2, Stream stream)
        {
            byte payload_len_flag = 0x7F;
            uint len = (uint)(byte_2 & payload_len_flag);

            // read a short length or a long length depending on the value of len
            if (len == 126)
            {
                len = stream.read_ushort(false);
            }
            else if (len == 127)
            {
                len = stream.read_uint(false);
                const uint maxLen = 2147483648; // 2GB

                // protect ourselves against bad data
                if (len > maxLen || len < 0)
                {
                    throw new ArgumentOutOfRangeException(string.Format("Payload length out of range. Min 0 max 2GB. Actual {0:#,##0} bytes.", len));
                }
            }

            return len;
        }

        private const string ws_guid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
        private string compute_accept_string(string key)
        {
            return Convert.ToBase64String(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(key + ws_guid)));
        }
        protected virtual void on_http_request(context context)
        { }
        protected virtual void on_websocket_connect(websocket socket)
        { }
        protected virtual void on_websocket_disconnect(websocket socket, string reason)
        {}
        protected virtual void on_websocket_pong(websocket socket, string message)
        {}
        protected virtual void on_message(websocket socket, string message, bool last_frame)
        { }
        protected virtual void on_data(websocket socket, byte[] data, bool last_frame)
        { }
    }
}
