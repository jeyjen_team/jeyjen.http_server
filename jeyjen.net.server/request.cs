﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace jeyjen.net.server
{
    public class request
    {
        private connection _con;
        private string _method;
        private string _url;
        private string _host;
        private string _origin;
        private string _version;
        private byte[] _body;
        private ReadOnlyDictionary<string, string> _headers;
        private ReadOnlyDictionary<string, string> _url_params;
        private ReadOnlyDictionary<string, string> _cookie;

        public request(string method, string url, string version, Dictionary<string, string> param, Dictionary<string, string> header, byte[] body)
        {
            _method = method;
            _url = url;
            _version = version;
            _url_params = new ReadOnlyDictionary<string, string>(param);
            _host = "";
            _body = body;
            if (header.ContainsKey("Host"))
            {
                _host = header["Host"];
                header.Remove("Host");
            }
            if (header.ContainsKey("Origin"))
            {
                _origin = header["Origin"];
                header.Remove("Origin");
            }
            else
            {
                _origin = _host;
            }

            var ck = new Dictionary<string, string>();
            if (header.ContainsKey("Cookie"))
            {
                var v = header["Cookie"];
                var cs = v.Split(';');
                foreach (var c in cs)
                {
                    var vs = c.Split('=');
                    ck.Add(vs[0].Trim(), vs[1].Trim());
                }
                header.Remove("Cookie");
            }
            _cookie = new ReadOnlyDictionary<string, string>(ck);
            _headers = new ReadOnlyDictionary<string, string>(header);
        }

        public connection connection
        {
            get
            {
                return _con;
            }
        }
        public string method
        {
            get
            {
                return _method;
            }
        }
        public string url
        {
            get
            {
                return _url;
            }
        }
        public string version
        {
            get
            {
                return _version;
            }
        }
        public string host
        {
            get
            {
                return _host;
            }
        }

        public string origin
        {
            get
            {
                return _origin;
            }
        }

        public ReadOnlyDictionary<string, string> url_param
        {
            get
            {
                return _url_params;
            }
        }
        public ReadOnlyDictionary<string, string> headers
        {
            get
            {
                return _headers;
            }
        }
        public byte[] body
        {
            get
            {
                return _body;
            }
        }
    }
}
