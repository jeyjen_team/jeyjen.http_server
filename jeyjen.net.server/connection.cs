﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.net.server
{
    public class connection: IDisposable
    {
        private TcpClient _client;
        private Stream _stream;
        private byte[] _buff;
        //private X509Certificate2 _client_certificate

        public connection(TcpClient client, X509Certificate2 certificate = null)
        {
            _client = client;
            _buff = new byte[1];
            if (certificate == null)
            {
                _stream = _client.GetStream();
            }
            else
            {
                try
                {
                    var ss = new SslStream(client.GetStream(), false);
                    //var ss = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateClientCertificate), new LocalCertificateSelectionCallback(cb));
                    ss.AuthenticateAsServer(certificate, false, SslProtocols.Tls, true);
                    _stream = ss;
                }
                catch (Exception ex)
                {
                    Dispose(); // алгоритм по корректному закрытию подключения и потока
                }
            }
        }

        public IPEndPoint remote
        {
            get
            {
                return ((IPEndPoint)_client.Client.RemoteEndPoint);
            }
        }

        public IPEndPoint local
        {
            get
            {
                return ((IPEndPoint)_client.Client.LocalEndPoint);
            }
        }
        
        public Stream stream
        {
            get
            {
                return _stream;
            }
        }

        internal TcpClient client
        {
            get
            {
                return _client;
            }
        }

        public void close()
        {
            Dispose();
        }

        public void Dispose()
        {
            try
            {
                _client.Client.Dispose();
                _client.Close();
            }
            catch (Exception e)
            {
            }
        }

        public bool is_alive
        {
            get
            {
                try
                {
                    if (_client != null && _client.Client != null && _client.Client.Connected)
                    {
                        if (_client.Client.Poll(0, SelectMode.SelectRead))
                        {
                           
                            if (_client.Client.Receive(_buff, SocketFlags.Peek) == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        #region maybe
        public static bool ValidateClientCertificate(
             object sender,
             X509Certificate certificate,
             X509Chain chain,
             SslPolicyErrors sslPolicyErrors)
        {
            return certificate != null;
        }

        public static X509Certificate cb(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return localCertificates[0];
        }
        #endregion
    }
}
