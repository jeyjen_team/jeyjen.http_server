﻿using jeyjen.extension;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace jeyjen.net.server
{
    public enum  ws_opcode : byte
    {
        continuation_frame = 0,
        text_frame = 1,
        binary_frame = 2,
        connection_close = 8,
        ping = 9,
        pong = 10
    }

    public class websocket
    {
        private connection _con;
        private string _url;
        private string _host;
        private string _origin;
        private string _version;
        private ReadOnlyDictionary<string, string> _url_params;
        private ReadOnlyDictionary<string, string> _cookies;
        private ReadOnlyCollection<string> _protocols;

        internal websocket(connection connection, string url, string host, string origin, Dictionary<string, string> url_params, Dictionary<string, string> headers)
        {
            _con = connection;
            _url = url;
            _version = headers.ContainsKey("Sec-WebSocket-Version") ? headers["Sec-WebSocket-Version"] : "";
            _host = host;
            _origin = origin;
            var ck = new Dictionary<string, string>();
            if (headers.ContainsKey("Cookie"))
            {
                var v = headers["Cookie"];
                var cs = v.Split(';');
                foreach (var c in cs)
                {
                    var vs = c.Split('=');
                    ck.Add(vs[0].Trim(), vs[1].Trim());
                }
            }
            _cookies = new ReadOnlyDictionary<string, string>(ck);
            _url_params = new ReadOnlyDictionary<string, string>(url_params);
            if (headers.ContainsKey("Sec-WebSocket-Protocol"))
            {
                _protocols = new ReadOnlyCollection<string>(headers["Sec-WebSocket-Protocol"].Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
            }
            else
            {
                _protocols = new ReadOnlyCollection<string>(new string[0]);
            }
        }
        public connection connection
        {
            get
            {
                return _con;
            }
        }
        public string url
        {
            get
            {
                return _url;
            }
        }
        public string host
        {
            get
            {
                return _host;
            }
        }

        public string origin
        {
            get
            {
                return _origin;
            }
        }

        public string version
        {
            get
            {
                return _version;
            }
        }

        public ReadOnlyDictionary<string, string> url_params
        {
            get
            {
                return _url_params;
            }
        }
        public ReadOnlyDictionary<string, string> cookies
        {
            get
            {
                return _cookies;
            }
        }
        public ReadOnlyCollection<string> protocols
        {
            get
            {
                return _protocols;
            }

        }

        public void send(ws_opcode op_code, byte[] payload, bool is_last_frame)
        {
            using (var memory_stream = new MemoryStream())
            {
                byte fin_bit_set_as_byte = is_last_frame ? (byte)0x80 : (byte)0x00;
                byte byte_1 = (byte)(fin_bit_set_as_byte | (byte)op_code);
                memory_stream.WriteByte(byte_1);

                // NB, dont set the mask flag. No need to mask data from server to client
                // depending on the size of the length we want to write it as a byte, ushort or ulong
                if (payload.Length < 126)
                {
                    byte byte_2 = (byte)payload.Length;
                    memory_stream.WriteByte(byte_2);
                }
                else if (payload.Length <= ushort.MaxValue)
                {
                    byte byte_2 = 126;
                    memory_stream.WriteByte(byte_2);
                    memory_stream.write_ushort((ushort)payload.Length, false);
                }
                else
                {
                    byte byte_2 = 127;
                    memory_stream.WriteByte(byte_2);
                    memory_stream.write_ulong((ulong)payload.LongLength, false);
                }
                memory_stream.Write(payload, 0, payload.Length);
                byte[] buffer = memory_stream.ToArray();
                _con.stream.Write(buffer, 0, buffer.Length);
            }
        }
        public void send(ws_opcode op_code, byte[] payload)
        {
            send(op_code, payload, true);
        }
        public void send(string text)
        {
            byte[] data = Encoding.UTF8.GetBytes(text);
            send(ws_opcode.text_frame, data);
        }
        public void send(byte[] data)
        {
            send(ws_opcode.binary_frame, data);
        }
        
        public void close(string reason = "")
        {
            send(ws_opcode.connection_close, Encoding.UTF8.GetBytes(reason));
        }

        public void ping(string text = "")
        {
            send(ws_opcode.ping, Encoding.UTF8.GetBytes(text));
        }
        
    }
}
