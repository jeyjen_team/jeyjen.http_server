﻿using jeyjen.extension;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jeyjen.net.server
{
    public class responce
    {
        #region static
        private static Dictionary<status, string> _reason_phrase;
        static responce()
        {
            #region status
            _reason_phrase = new Dictionary<status, string>();

            _reason_phrase.Add(status.Continue, "Continue");
            _reason_phrase.Add(status.Switching_Protocols, "Switching Protocols");
            _reason_phrase.Add(status.OK, "OK");
            _reason_phrase.Add(status.Created, "Created");
            _reason_phrase.Add(status.Accepted, "Accepted");
            _reason_phrase.Add(status.Non_Authoritative_Information, "Non-Authoritative Information");
            _reason_phrase.Add(status.No_Content, "No Content");
            _reason_phrase.Add(status.Reset_Content, "Reset Content");
            _reason_phrase.Add(status.Partial_Content, "Partial Content");
            _reason_phrase.Add(status.Multiple_Choices, "Multiple Choices");
            _reason_phrase.Add(status.Moved_Permanently, "Moved Permanently");
            _reason_phrase.Add(status.Found, "Found");
            _reason_phrase.Add(status.See_Other, "See Other");
            _reason_phrase.Add(status.Not_Modified, "Not Modified");
            _reason_phrase.Add(status.Use_Proxy, "Use Proxy");
            _reason_phrase.Add(status.Temporary_Redirect, "Temporary Redirect");
            _reason_phrase.Add(status.Bad_Request, "Bad Request");
            _reason_phrase.Add(status.Unauthorized, "Unauthorized");
            _reason_phrase.Add(status.Payment_Required, "Payment Required");
            _reason_phrase.Add(status.Forbidden, "Forbidden");
            _reason_phrase.Add(status.Not_Found, "Not Found");
            _reason_phrase.Add(status.Method_Not_Allowed, "Method Not Allowed");
            _reason_phrase.Add(status.Not_Acceptable, "Not Acceptable");
            _reason_phrase.Add(status.Proxy_Authentication_Required, "Proxy Authentication Required");
            _reason_phrase.Add(status.Request_Time_out, "Request Time-out");
            _reason_phrase.Add(status.Conflict, "Conflict");
            _reason_phrase.Add(status.Gone, "Gone");
            _reason_phrase.Add(status.Length_Required, "Length Required");
            _reason_phrase.Add(status.Precondition_Failed, "Precondition Failed");
            _reason_phrase.Add(status.Request_Entity_Too_Large, "Request Entity Too Large");
            _reason_phrase.Add(status.Request_URI_Too_Large, "Request-URI Too Large");
            _reason_phrase.Add(status.Unsupported_Media_Type, "Unsupported Media Type");
            _reason_phrase.Add(status.Requested_range_not_satisfiable, "Requested range not satisfiable");
            _reason_phrase.Add(status.Expectation_Failed, "Expectation Failed");
            _reason_phrase.Add(status.Internal_Server_Error, "Internal Server Error");
            _reason_phrase.Add(status.Not_Implemented, "Not Implemented");
            _reason_phrase.Add(status.Bad_Gateway, "Bad Gateway");
            _reason_phrase.Add(status.Service_Unavailable, "Service Unavailable");
            _reason_phrase.Add(status.Gateway_Time_out, "Time-out");
            _reason_phrase.Add(status.HTTP_Version_not_supported, "HTTP Version not supported");
            #endregion
        }
        #endregion
        private connection _con;
        private string _version;
        private status _status;
        private Dictionary<string, string> _header;
        private string _content_type;
        private bool _is_used;

        public status status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        public string version
        {
            get
            {
                return _version;
            }
        }
        public ReadOnlyDictionary<string, string> headers
        {
            get
            {
                return new ReadOnlyDictionary<string, string>(_header);
            }
        }

        public string content_type
        {
            get
            {
                return _content_type;
            }
            set
            {
                _content_type = value;
            }
        }

        public responce(connection connection, string version)
        {
            _con = connection;
            _version = version;
            _status = status.OK;
            _content_type = "";
            _header = new Dictionary<string, string>();
            _is_used = false;
        }

        public void send(string body)
        {
            send(Encoding.UTF8.GetBytes(body));
        }

        public void send(byte[] body = null)
        {
            if (_is_used == true)
            {
                throw new Exception("responce object has been used to send data to client");
            }

            long add_lenght = (body == null) ? 0 : body.LongLength;
            _header.Add("Content-Length", add_lenght.ToString());
            if (! _content_type.is_null_or_empty())
            {
                if (_header.ContainsKey("Content-Type"))
                {
                    _header.Remove("Content-Type");
                }
                _header.Add("Content-Type", content_type);
            }
            var resp = new StringBuilder();
            resp.AppendFormat("{0} {1} {2}\r\n", _version, (int)_status, _reason_phrase[_status]);

            foreach (var h in _header)
            {
                resp.AppendFormat("{0}: {1}\r\n", h.Key, h.Value);
            }
            resp.Append("\r\n");

            var hb = Encoding.ASCII.GetBytes(resp.ToString());
            var r = new byte[hb.Length + add_lenght];
            Array.Copy(hb, 0, r, 0, hb.Length);
            if (add_lenght > 0)
            {
                Array.Copy(body, 0, r, hb.Length, body.Length);
            }
            _con.stream.Write(r, 0, r.Length);
            _con.stream.Flush();
            _is_used = true;
        }

        public string this[string header]
        {
            get
            {
                return _header[header];
            }
            set
            {
                if (_header.ContainsKey(header))
                {
                    _header[header] = _header[header] + ", " + value;
                }
                else
                {
                    _header.Add(header, value);
                }
            }
        }
    }
}
