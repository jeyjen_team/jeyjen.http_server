﻿namespace jeyjen.net.server
{
    public class context
    {
        private request _request;
        private responce _responce;
        private connection _con;

        internal context(connection connection, request request, responce responce)
        {
            _con = connection;
            _request = request;
            _responce = responce;
        }
        public connection connection
        {
            get
            {
                return _con;
            }
        }

        public request request
        {
            get
            {
                return _request;
            }
        }

        public responce responce
        {
            get
            {
                return _responce;
            }
        }
        public void close()
        {
            _con.close();
        }
    }
}
